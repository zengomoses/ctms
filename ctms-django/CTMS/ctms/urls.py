from django.contrib import admin
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from authentication import views

urlpatterns = [
   url(r'^admin/', admin.site.urls),
   url(r'^$', auth_views.login, {'template_name': 'login.html'}, name='login'),
   url(r'^home$', views.index, name='index'),
   url(r'^location$', views.location, name='location'),
   url(r'^driver$', views.driver, name='drivers'),
   url(r'vehicle$', views.vehicle, name='vehicles'),
   url(r'^route$', views.route, name='routes'),
   url(r'^report$', views.report, name='reports'),
   url(r'^asset$', views.asset, name='assets'),
   url(r'^geofence$', views.geofence, name='geofence'),
   url(r'^schedule$', views.schedule, name='schedules'),
]
