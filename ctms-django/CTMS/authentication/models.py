from django.db import models
from django.contrib.gis.db import models
from django.contrib.auth.models import User

# Create your models here.

class User(models.Model):
	username=models.CharField(max_length=40)
	password=models.CharField(max_length=50)
	type=models.TextField()

	class Meta:
		verbose_name_plural='User'
		db_table='user'
		
class UserProfile(models.Model):
	fname=models.CharField(max_length=40)
	lname=models.CharField(max_length=40)
	email=models.EmailField()
	phone=models.IntegerField()
	UseId= models.ForeignKey(User, on_delete=models.CASCADE)

	class Meta:
		verbose_name_plural='User'
		db_table='user'


class Driver(models.Model):
	fname=models.CharField(max_length=40)
	lname=models.CharField(max_length=40)
	email=models.EmailField()
	phone=models.IntegerField()

	class Meta:
		verbose_name_plural='Driver'
		db_table='driver'

class Journey(models.Model):
	jfrom=models.CharField(max_length=40)
	to=models.CharField(max_length=40)
	distance=models.IntegerField()
	duration=models.DateField()

	class Meta:
		verbose_name_plural='Journey'
		db_table='journey'

class Movement(models.Model):
	start=models.IntegerField()
	date=models.DateField(max_length=40)
	time=models.IntegerField()

	class Meta:
		verbose_name_plural='Movement'
		db_table='movement'





		

