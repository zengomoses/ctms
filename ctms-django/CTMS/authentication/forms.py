from django import forms
from django.contrib.auth.models import User

class loginForm(forms.ModelForm):
    username = forms.CharField(
        widget = forms.TextInput(attrs = {'class': 'form-control'}),
		max_length = 30,
		required = True
    )
	password = forms.CharField(
		widget = forms.PasswordInput(attrs = {'class': 'form-control'}),
		max_length = 30,
		required = True 
	)