from django.shortcuts import render
from django.contrib.auth import authenticate, login

def index(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		login(request, user)
		return render(request, "index.html")
	else:
		return render(request, 'login.html')
	return render(request, "index.html")
def location(request):
    return render(request, "location.html")

def driver(request):
	return render(request, "driver.html")

def route(request):
	return render(request, "routes.html")

def vehicle(request):
	return render(request, "vehicles.html")

def geofence(request):
	return render(request, "geofence.html")

def report(request):
	return render(request, "reports.html")

def asset(request):
	return render(request, "assets.html")

def schedule(request):
	return render(request, "schedules.html")

